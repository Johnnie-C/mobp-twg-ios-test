//
//  UtilsTest.swift
//  The Warehouse TestTests
//
//  Created by Johnnie Cheng on 21/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import XCTest
@testable import The_Warehouse_Test

class UtilsTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAppendPrefix() {
        var str = "a"
        str = str.appendPrefix("b")
        XCTAssertEqual(str, "ba")
        
        str = "a"
        str = str.appendPrefix("")
        XCTAssertEqual(str, "a")
    }

    func testAppendSuffix() {
        var str = "a"
        str = str.appendSuffix("b")
        XCTAssertEqual(str, "ab")
        
        str = "a"
        str = str.appendSuffix("")
        XCTAssertEqual(str, "a")
    }
    
    func testPureClassName_Swift_Class() {
        let className = NSStringFromClass(DIComponent.self.classForCoder())
        XCTAssertEqual(className, "The_Warehouse_Test.DIComponent")
        
        let pureClassName = DIComponent.pureClassName()
        XCTAssertEqual(pureClassName, "DIComponent")
    }
    
    func testPureClassName_OC_Class() {
        let className = NSStringFromClass(AppDelegate.self.classForCoder())
        XCTAssertEqual(className, "AppDelegate")
        
        let pureClassName = AppDelegate.pureClassName()
        XCTAssertEqual(pureClassName, "AppDelegate")
    }

}
