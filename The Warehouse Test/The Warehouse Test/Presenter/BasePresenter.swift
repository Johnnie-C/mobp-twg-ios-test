//
//  BasePresenter.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import Alamofire

protocol BaseView: class{
    func startLoading(title: String?);
    func stopLoading();
    func showErrorMessage(errorMsg: String?);
}




class BasePresenter<View: BaseView>: NSObject {
    
    public weak var view: View?
  
  
    public func onError() ->  ((Error) -> Void){
        return {error in
            let message = type(of: error) == AFError.self ?
                String.stringWithKey("general_err_msg") : error.localizedDescription
          
            self.view?.stopLoading()
            self.view?.showErrorMessage(errorMsg: message)
        }
    }
    
}
