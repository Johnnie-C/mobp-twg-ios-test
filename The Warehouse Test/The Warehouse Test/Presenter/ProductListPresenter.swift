//
//  ProductListPresenter.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import RxSwift

protocol ProductListView: BaseView {
    func onProductsSearched(products: [Product])
}





class ProductListPresenter<View: ProductListView>: BasePresenter<View> {
    
    var currentSearchRequest: Disposable?
    let productInteractor: ProductInteractor
  
  
    init(productInteractor: ProductInteractor){
        self.productInteractor = productInteractor
    }
    
    func searchProducts(branch: Int, start: Int, limit: Int, searchText: String){
        if let currentSearchRequest = currentSearchRequest{
            currentSearchRequest.dispose()
        }
        
        currentSearchRequest = productInteractor.searchProducts(branch: branch, start: start, limit: limit, searchText: searchText)
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {response in
                if let response = response as? SearchProductResponse {
                    self.view?.onProductsSearched(products: response.products)
                }
            }, onError:onError())

    }
    
    func cancelSearch(){
        if let currentSearchRequest = currentSearchRequest{
            currentSearchRequest.dispose()
        }
    }
  
}
