//
//  ProductDetailPresenter.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import RxSwift

protocol ProductDetailView: BaseView {
    func onProductPrepared(product: Product?)
}





class ProductDetailPresenter<View: ProductDetailView>: BasePresenter<View> {
    
    var currentSearchRequest: Disposable?
    let productInteractor: ProductInteractor
  
  
    init(productInteractor: ProductInteractor){
        self.productInteractor = productInteractor
    }
    
    public func getProductPrice(branch: Int, barcode: String?) {
        if let currentSearchRequest = currentSearchRequest{
            currentSearchRequest.dispose()
        }
        
        guard let barcode = barcode else {
            self.view?.onProductPrepared(product: nil)
            return
        }
        
        currentSearchRequest = productInteractor.getProductPrice(branch:branch, barcode: barcode)
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {response in
                if let response = response as? ProductPriceResponse {
                    self.view?.onProductPrepared(product: response.product)
                }
            }, onError:onError())
    }

}
