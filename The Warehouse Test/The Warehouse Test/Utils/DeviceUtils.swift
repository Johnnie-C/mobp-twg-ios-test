//
//  DeviceUtils.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class DeviceUtils: NSObject {
    
    private static let KEY_DEVICE_ID = "key_deviceID"
  
  
    /// This value will change for each install
    /// Consider saving it in keychain to avoid changing if need
    ///
    /// - Returns:
    public static func deviceID() -> String {
        var deciveID = UserDefaults.standard.string(forKey: DeviceUtils.KEY_DEVICE_ID)
        if deciveID == nil {
            deciveID = UIDevice.current.identifierForVendor?.uuidString
        }
        return deciveID!
    }

}
