//
//  UIColor+TWG.h
//  
//
//  Created by Johnnie Cheng on 18/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

#import "UIColor+TWG.h"
#import "UIColor+JCUtils.h"


@implementation UIColor(TWG)
    
+ (UIColor *)appMainColor{
    return [UIColor colorWithRed:189.0/255.0 green:0.0/255.0 blue:3.0/255.0 alpha:1];
}
    
+ (UIColor *)appMainColorLight{
    return [[UIColor appMainColor] lighterColor];
}
    
@end
