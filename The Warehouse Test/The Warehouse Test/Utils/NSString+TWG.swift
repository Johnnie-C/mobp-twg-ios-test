//
//  NSString+TWG.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 21/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit

extension String{
  
  public static func stringWithKey(_ key: String, _ args: CVarArg...) -> String {
    var str = NSLocalizedString(key, comment: "");
    
    if !args.isEmpty {
      str = String(format: str, locale: .current, arguments: args)
    }
    
    return str
  }
  
}
