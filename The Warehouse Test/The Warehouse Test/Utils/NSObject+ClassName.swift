//
//  NSObject+ClassName.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 18/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit

extension NSObject {
    
    /// get pure name of a class without namespace
    ///
    /// - Returns: pure name
    static func pureClassName() -> String {
        var className = NSStringFromClass(self.classForCoder())
        if let pureClassName = className.components(separatedBy: ".").last {
            className = pureClassName
        }
        
        return className
    }
  
}
