//
//  UIColor+TWG.h
//
//
//  Created by Johnnie Cheng on 18/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

// Pods are compiled before app source code,
// So we are able to override category here from JCUtils/UIColor+JCUtils
// Category has to be overried in Objective-C only


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor(TWG)
    
+ (UIColor *)appMainColor;
    
@end

