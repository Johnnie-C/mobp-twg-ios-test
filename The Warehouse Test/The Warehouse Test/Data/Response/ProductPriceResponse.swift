//
//  ProductPriceResponse.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class ProductPriceResponse: NSObject {
    
    var product: Product? = nil
  
  
    convenience init(dict: [AnyHashable : Any]?){
        self.init()
        if let dict = dict,
           let productDict = dict["Product"] as? [AnyHashable : Any]
        {
            product = Product(dict: productDict)
        }
    }
}
