//
//  SearchProductResponse.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class SearchProductResponse: NSObject {
    
    var products = [Product]()
  
  
    convenience init(dict: [AnyHashable : Any]?){
        self.init()
        if let dict = dict,
           let resultDicts = dict["Results"] as? [[AnyHashable : Any]]
        {
            // Data:
            //{   "Results": [ "Products" : [{xxx}] ]   }
            //
            // Not sure about relationship between "Results" and "Products"
            // so here just simply takes all products and ignore "Results"
            for resultDict in resultDicts {
                if let productDicts = resultDict["Products"] as? [[AnyHashable : Any]]{
                    for productDict in productDicts {
                        products.append(Product(dict: productDict))
                    }
                }
            }
        }
    }
}
