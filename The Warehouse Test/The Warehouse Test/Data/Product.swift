//
//  Product.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit

class Product: NSObject {
    
    var productKey: String?
    var imageUrl: String?
    var title: String?
    var desc: String?
    var category: String?
    var subCategory: String?
    var priceObj: Price?
    var barcode: String?
    var branchPrice: Float = 0.0
    
    convenience init(dict: [AnyHashable : Any]?){
        self.init()
        
        if let dict = dict {
            productKey = dict["ProductKey"] as? String
            category = dict["Class0"] as? String
            subCategory = dict["SubClass"] as? String
            imageUrl = dict["ImageURL"] as? String
            
            //assume using as title, it is the only field looks like title
            title = dict["Description"] as? String
            
            //assume using as original price
            if let branchPriceStr = dict["BranchPrice"] as? String {
                branchPrice = Float(branchPriceStr) ?? 0.0
            }
            
            desc = dict["Description"] as? String
            barcode = dict["Barcode"] as? String
            
            if let priceDict = dict["Price"] as? [AnyHashable : Any]{
                priceObj = Price(dict: priceDict)
            }
            
        }
    }
    
    public func hasDiscount() -> Bool {
        return ["adv", "grp", "sto", "clr"].contains(priceObj?.type?.lowercased())
    }
  
    public func discountTitle() -> String? {
        var discountTitle: String? = nil
        switch priceObj?.type?.lowercased() {
        case "adv":
            discountTitle = String.stringWithKey("price_drop")
            break
            
        case "grp":
            discountTitle = String.stringWithKey("price_drop")
            break
            
        case "sto":
            discountTitle = String.stringWithKey("special")
            break
            
        case "clr":
            discountTitle = String.stringWithKey("clearance")
            break
            
        default:
            break
            
        }
        return discountTitle
    }
    
}





class Price: NSObject{
    var price: Float = 0.0
    var type: String?
    
    
    convenience init(dict: [AnyHashable : Any]?){
        self.init()
        
        if let dict = dict {
            type = dict["Type"] as? String
            
            if let priceStr = dict["Price"] as? String {
                price = Float(priceStr) ?? 0.0
            }
        }
    }
}
