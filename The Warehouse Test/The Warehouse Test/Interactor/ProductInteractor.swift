//
//  ProductInteractor.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 20/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import RxSwift

class ProductInteractor: NSObject {
    
    let api: API
    
    init(api: API){
        self.api = api
    }
    
    
    public func searchProducts(branch: Int, start: Int, limit: Int, searchText: String)
        -> RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any>
    {
        return api.searchProducts(branch: branch,
                                  start: start,
                                  limit: limit,
                                  searchText: searchText)
            .map({ json -> SearchProductResponse in
                return SearchProductResponse(dict: json as? [AnyHashable : Any])
            })
    }
    
    public func getProductPrice(branch: Int, barcode: String)
        ->  RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any>
    {
        return api.getProductPrice(branch: branch, barcode:barcode)
            .map({ json -> ProductPriceResponse in
                return ProductPriceResponse(dict: json as? [AnyHashable : Any])
            })
    }
  
}
