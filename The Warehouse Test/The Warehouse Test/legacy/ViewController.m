//
//  ViewController.m
//  The Warehouse Test
//
//  Created by John Douglas on 10/10/18.
//  Copyright © 2018 The Warehouse Group Limited. All rights reserved.
//

#import "ViewController.h"
#import "NSString+Utils.h"
#import <JCUtils/JCToast.h>
#import <JCUtils/JCFramework.h>

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIButton *sBB;
@property (strong, nonatomic) UISearchController *sContrllr;

@end





@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.sBB setTitle:[self buildScanbtnTitle] forState:UIControlStateNormal];
    
    //navigation bar
    [self setLeftBarButtonType:LeftBarButtonTypeBack];
    self.title = @"Legacy ViewController";
    
    //deprecated warning
    [JCToast toastWithMessage:@"This is deprecated" colour:[UIColor toastMessageOrange]];
}

- (NSString *)buildScanbtnTitle {
    NSString *btnTitle = [NSString stringWithFormat:@"%@%@%@", @" ", self.sBB.currentTitle  ,@" "];
    
    NSInteger limit = 11;
    for (int i = 0; i < limit; i++) {
        if (i == limit - 1) {
            btnTitle = [NSString stringWithFormat:@"%@%@%@", @"😬", btnTitle  ,@"😬"];
        } else {
            if (i % 3 == 0) {
                btnTitle = [btnTitle appendPrefix:@"📸"];
            } else if (i % 3 == 1) {
                btnTitle = [btnTitle appendSuffix:@"📸"];
            } else {
                btnTitle = [NSString stringWithFormat:@"%@%@%@", @"💯 ", btnTitle  ,@" 💯"];
            }
        }
    }
    
    return btnTitle;
}

- (IBAction)scanBarcodeButtonPressed:(id)sender {
    [self popViewController];
    if(_delegate && [_delegate respondsToSelector:@selector(onScanClicked)]){
        [_delegate onScanClicked];
    }
}

@end

