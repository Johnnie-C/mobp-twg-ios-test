//
//  NSString+Utils.h
//  The Warehouse Test
//
//  Created by John Douglas on 10/10/18.
//  Copyright © 2018 The Warehouse Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Utils)

- (NSString *)appendPrefix:(NSString *)prefix;
- (NSString *)appendSuffix:(NSString *)suffix;

@end

NS_ASSUME_NONNULL_END
