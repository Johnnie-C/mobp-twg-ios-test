//
//  NSString+Utils.m
//  The Warehouse Test
//
//  Created by John Douglas on 10/10/18.
//  Copyright © 2018 The Warehouse Group Limited. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (NSString *)appendPrefix:(NSString *)prefix {
  return [prefix stringByAppendingString:self];
}

- (NSString *)appendSuffix:(NSString *)suffix {
  return [self stringByAppendingString:suffix];
}

@end
