//
//  ViewController.h
//  The Warehouse Test
//
//  Created by John Douglas on 10/10/18.
//  Copyright © 2018 The Warehouse Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JCUtils/JCDesign.h>

@protocol ViewControllerDelegate <NSObject>

- (void)onScanClicked;

@end


@interface ViewController : JCBaseViewController

@property (nonatomic, weak) NSObject<ViewControllerDelegate> *delegate;

@end

