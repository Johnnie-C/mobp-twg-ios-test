//
//  API.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import RxAlamofire

protocol API: class {
    func searchProducts(branch: Int, start: Int, limit: Int, searchText: String)
        ->  RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any>
    
    func getProductPrice(branch: Int, barcode: String)
        ->  RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any>
}


class APIImplement: NSObject, API {
    
    static let KEY_USER_ID = "key_userID"
    
    static let API_KEY = "8592aa3bbf16415abff0800053756e2c"
    static let HOST = "https://twg.azure-api.net/bolt/"
    static let END_POINT_NEW_USER = "newuser.json"
    static let END_POINT_SEARCH = "search.json"
    static let END_POINT_PRICE = "price.json"
  
    var afManager: SessionManager
  
  
    override init(){
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["Ocp-Apim-Subscription-Key":APIImplement.API_KEY]
        afManager = Alamofire.SessionManager(configuration: configuration)
        
    }
   
    public func searchProducts(branch: Int,
                               start: Int,
                               limit: Int,
                               searchText: String)
        ->  RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any>
    {
        return performRequest(.get,
                              getRequestURL(endPoint: APIImplement.END_POINT_SEARCH),
                              parameters: ["Start":start,
                                           "Limit":limit,
                                           "Branch":branch,
                                           "Search":searchText])
    }
    
    public func getProductPrice(branch: Int,
                               barcode: String)
        ->  RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any>
    {
        return performRequest(.get,
                              getRequestURL(endPoint: APIImplement.END_POINT_PRICE),
                              parameters: ["Barcode":barcode,
                                           "Branch":branch])
    }
  
  
    //MARK: - low level requset
    func getRequestURL(endPoint: String) -> String{
        return "\(APIImplement.HOST)\(endPoint)"
    }

    ///
    /// A wrapper of [SessionManager.rx.json(...)]
    /// Always call this function to perform request, this guarantee [UserID] and [MachineID] is fetched
    ///
    /// - Parameters:
    ///   - method: Alamofire method object
    ///   - url: An object adopting `URLConvertible`
    ///   - parameters: A dictionary containing all necessary options
    ///   - encoding: The kind of encoding used to process parameters
    ///   - headers: A dictionary containing all the additional headers
    /// - Returns: Response in [Single]
    private func performRequest( _ method: Alamofire.HTTPMethod,
                                 _ url: URLConvertible,
                                 parameters: [String: Any]? = nil,
                                 encoding: ParameterEncoding = URLEncoding.default,
                                 headers: [String: String]? = nil)
        -> RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any>
    {
        return requestUserID()
            .flatMap { (userID) -> RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Any> in
                var realParameters = parameters ?? [String: Any]()
                realParameters.updateValue(userID!, forKey: "UserID")
                realParameters.updateValue(DeviceUtils.deviceID(), forKey: "MachineID")
                
                return self.afManager.rx.json(method,
                                         url,
                                         parameters:realParameters,
                                         encoding: encoding,
                                         headers: headers)
                    .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                    .debug()
                    .asSingle()
        }
    }
    
  
    //MARK: - UserID
    private func requestUserID() -> RxSwift.PrimitiveSequence<RxSwift.SingleTrait, String?>{
        var ob: RxSwift.PrimitiveSequence<RxSwift.SingleTrait, String?>
        let userID = UserDefaults.standard.string(forKey: APIImplement.KEY_USER_ID)
        if (!(userID ?? "").isEmpty){
            ob = Single.just(userID)
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
        }
        else{
            return afManager.rx.json(.get,
                                     getRequestURL(endPoint: APIImplement.END_POINT_NEW_USER),
                                     parameters: ["MachineID":DeviceUtils.deviceID()])
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .map({ (json) -> String in
                    var userID = ""
                    if let dict = json as? [AnyHashable:Any]{
                        userID = dict["UserID"] as? String ?? ""
                    }
                    if(!userID.isEmpty){
                        UserDefaults.standard.setValue(userID, forKey: APIImplement.KEY_USER_ID)
                    }
                    else{
                        throw APIError.UserIDError(String.stringWithKey("general_err_msg"))
                    }
                    
                    return userID
                })
                .asSingle()
        }
        
        return ob
    }
  
}


enum APIError: Error {
    case UserIDError(String)
}
