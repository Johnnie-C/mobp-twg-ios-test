//
//  JCBarButtonItem+TWG.h
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//


#import <JCUtils/JCDesign.h>

// Add enum item to Objecti-C code for custermised Navigation bar buttons
// Type must be to [LeftBarButtonType] or [RightBarButtonType]
// and value should be defined in [JCBarButtonItem+TWG.m] with a number > 1000 to avoid conflict
extern const LeftBarButtonType LeftBarButtonTypeScan;
extern const RightBarButtonType RightBarButtonTypeLegacy;
