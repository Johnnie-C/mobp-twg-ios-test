//
//  BaseViewController.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 18/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: JCBaseViewController, BaseView {
    
    var navLabel: JCLabel? = nil
    var loadingView: NVActivityIndicatorView?
  

    convenience init(){
        self.init(nib: type(of:self).pureClassName())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup navigation bar
        setTitle()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.navigationBar.shadowImage = UIImage(color: UIColor.navigationbarBackground(), size: CGSize(width: JCUtils.screenWidth(), height: 1))
        
        //setup loading view
        loadingView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height: 30),
                                              type: .ballPulseSync,
                                              color: UIColor.appMain(),
                                              padding: 0)
        view.addSubview(loadingView!)
        loadingView?.addCenterVerticalConstraint(withOffset: 0)
        loadingView?.addCenterHorizontalConstraint(withOffset: 0)
        loadingView?.addWidthConstraint(30)
        loadingView?.addHeightConstraint(30)
    }

    
    //MARK: - Navigation bar
    open func prefersLargeTitles() -> Bool {
        return false
    }
    
    public func setTitle(_ title: String? = nil){
        if #available(iOS 11.0, *) {
            if let navigationVC = self.navigationController,
                prefersLargeTitles(),
                navigationVC.navigationItem.largeTitleDisplayMode != .never
            {
                self.title = title ?? "thewarehouse"
                return
            }
        }
        
        if(navLabel == nil){
            navLabel = JCLabel()
            navLabel?.textColor = UIColor.navigationbarText()
            self.navigationItem.titleView = navLabel
        }
        
        if((title ?? "").isEmpty){
            let navTitle = NSMutableAttributedString(string: "the", attributes:[
                NSAttributedStringKey.foregroundColor: UIColor.navigationbarText(),
                NSAttributedStringKey.font: UIFont.systemFont(ofSize: 21.0, weight: .light)])
            
            navTitle.append(NSMutableAttributedString(string: "warehouse", attributes:[
                NSAttributedStringKey.foregroundColor: UIColor.navigationbarText(),
                NSAttributedStringKey.font: UIFont.systemFont(ofSize: 21.0, weight: .heavy)]))
            
            navLabel?.attributedText = navTitle
            navLabel?.sizeToFit()
        }
        else{
            navLabel?.text = title
            navLabel?.sizeToFit()
        }
    }
    
    //MARK: -- Navigation bar items
    
    
    /// create bar item for left navigation bar on a given type
    /// type is set from self.setLeftBarButtonType(type) in child class
    /// JCUtils originally supports:
    /// [LeftBarButtonTypeNone, LeftBarButtonTypeBack, LeftBarButtonTypeClose, LeftBarButtonTypeMenu, LeftBarButtonTypeCancel]
    ///
    /// Only need override this function when use button types which are not originally supported
    ///
    /// This isn't the best way of generating cutomer item, but it is the min modification on my exist framework
    ///
    /// - Parameter type: left button type
    /// - Returns: JCBarButtonItem
    override open func createLeftBarItem(_ type: LeftBarButtonType) -> JCBarButtonItem {
        var item: JCBarButtonItem
        if(type.rawValue > 1000){
            item = JCBarButtonItem(leftBarButtonType: type, textBlock: { () -> String? in
                var text: String?
                return text
            }, iconBlock: { () -> UIImage? in
                var image: UIImage? = nil
                if(type.rawValue == LeftBarButtonTypeScan.rawValue){
                    image = UIImage(named: "ic_scan")
                }
                return image
            })

        }
        else{
            item = JCBarButtonItem(leftBarButtonType: type)
        }
        
        return item
    }
    
    
    /// create bar item for right navigation bar on a given type
    /// type is set from self.setRightBarButtonTypes([types]) in child class
    /// JCUtils originally supports:
    /// [RightBarButtonTypeNone, RightBarButtonTypeSearch, RightBarButtonTypeMenu, RightBarButtonTypeAdd, RightBarButtonTypeChange, RightBarButtonTypeCart]
    ///
    /// Only need override this function when use button types which are not originally supported
    ///
    /// This isn't the best way of generating cutomer item, but it is the min modification on my exist framework
    ///
    /// - Parameter type: left button type
    /// - Returns: JCBarButtonItem
    override open func createRightBarItem(_ type: RightBarButtonType) -> JCBarButtonItem {
        var item: JCBarButtonItem
        if(type.rawValue > 1000){
            item = JCBarButtonItem(rightBarButtonType: type, textBlock: { () -> String? in
                var text: String?
                if(type.rawValue == RightBarButtonTypeLegacy.rawValue){
                    text = "Legacy"
                }
                return text
            }) { () -> UIImage? in
                var image: UIImage? = nil
                if(type.rawValue == LeftBarButtonTypeScan.rawValue){
                    image = UIImage(named: "ic_scan")
                }
                return image
            }
        }
        else{
            item = JCBarButtonItem(rightBarButtonType: type)
        }
        
        return item
    }
    
    
    //MARK: - BaseView
    func startLoading(title: String?) {
        //consider using https://github.com/Juanpe/SkeletonView once it is compatible with RxCocoa tableview
        view.bringSubview(toFront: loadingView!)
        loadingView?.startAnimating()
    }
    
    func stopLoading() {
        loadingView?.stopAnimating()
    }
    
    func showErrorMessage(errorMsg: String?) {
        JCToast.toast(withMessage: errorMsg, colour: UIColor.appMain())
    }
    
}
