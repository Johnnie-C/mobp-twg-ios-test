//
//  JCBarButtonItem+TWG.m
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

#import "JCBarButtonItem+TWG.h"

//left
const LeftBarButtonType LeftBarButtonTypeScan = 1001;

//right
const RightBarButtonType RightBarButtonTypeLegacy = 1101;
