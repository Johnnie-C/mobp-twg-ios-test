//
//  ProductDetailViewController.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//


// Should use UITableView if we have a complex UI for product detail page
// Since this is a test, I have demonstrated the usage of table in [ProductListViewController]
// So here I am demonstrating the usage of UIScrollView with auto layout


import UIKit
import RxCocoa
import RxSwift
import SDWebImage

class ProductDetailViewController: BaseViewController, ProductDetailView {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lbDiscount: JCLabel!
    @IBOutlet weak var lbPrice: JCLabel!
    @IBOutlet weak var lbTitle: JCLabel!
    @IBOutlet weak var lbCategory: JCLabel!
    @IBOutlet weak var lbDesc: JCLabel!
    @IBOutlet weak var const_contentHeight: NSLayoutConstraint!
    
    let presetner: ProductDetailPresenter<ProductDetailViewController> = DIComponent.productDetailPresenter()
    
    var branch: Int? = 0
    var barcode: String?
    var product: Product?
  
  
    convenience init(branch: Int, barcode: String){
        self.init()
        self.branch = branch
        self.barcode = barcode
    }
    
    convenience init(branch: Int, product: Product){
        self.init()
        self.branch = branch
        self.product = product
        self.barcode = product.barcode
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        presetner.view = self
        presetner.getProductPrice(branch: branch!, barcode: barcode!)
        startLoading(title: nil)
    }
    
    func setupView(){
        setupNavBar()
        updateUI()
        
        let corners = UIRectCorner.topRight.union(UIRectCorner.bottomRight)
        lbDiscount.roundCorner(withRadius: lbDiscount.height() / 2, corners:corners)
    }
    
    func setupNavBar(){
        self.setLeftBarButtonType(.back)
    }

    func updateUI(){
        if let imageUrl = product?.imageUrl {
            ivIcon.sd_setImage(with: URL(string: imageUrl),
                               placeholderImage: UIImage(named: "placeholder"))
        }
        
        if let priceObj = product?.priceObj {
            lbPrice.text = String(format: "$%.2f", priceObj.price)
        }
        
        lbTitle.text = product?.title
        lbCategory.text = product?.category
        lbDiscount.isHidden = !(product?.hasDiscount() ?? false)
        lbDiscount.text = product?.discountTitle()
        
        if let desc = product?.desc {
            let attrDesc = NSMutableAttributedString(string: String.stringWithKey("product_desc"), attributes:[NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 17.0)])
            
            attrDesc.append(NSMutableAttributedString(string: "\n\n\(desc)", attributes:[NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17.0)]))
            
            lbDesc.attributedText = attrDesc
        }
        
        const_contentHeight.constant = lbDesc.bottom() + 20
    }

  
    //MARK: - ProductDetailView
    func onProductPrepared(product: Product?) {
        stopLoading()
        if let product = product {
            self.product = product
            updateUI()
        }
        else{
            JCUIAlertUtils.showConfirmDialog(String.stringWithKey("no_barcode_result", barcode ?? ""), content: "", okBtnTitle: String.stringWithKey("ok")) { (action) in
                self.pop()
            }
        }
    }

}
