//
//  ProductListCell.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 19/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import SDWebImage

class ProductListCell: UITableViewCell {

    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var lbTitle: JCLabel!
    @IBOutlet weak var lbCategory: JCLabel!

    
    public func updateUIWithProduct(_ product: Product){
        lbTitle.text = product.title
        lbCategory.text = product.category
        
        if let imageUrl = product.imageUrl {
            ivIcon.sd_setImage(with: URL(string: imageUrl),
                               placeholderImage: UIImage(named: "placeholder"))
        }
    }
    
}
