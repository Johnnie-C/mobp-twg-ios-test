//
//  ProductListViewController.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 18/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import BarcodeScanner
import RxSwift
import RxCocoa

@objcMembers
class ProductListViewController: BaseViewController, ProductListView, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate, ViewControllerDelegate {
  
    private static let ITEM_PER_PAGE = 20
    private static let CELL_IDENTIFIER = "ProductListCell"
    
    @IBOutlet weak var productsList: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var lbPlaceholderText: JCLabel!
    @IBOutlet weak var ivPlaceholderImage: UIImageView!
    
    var branch: Int? = 0
    let disposeBag = DisposeBag()
    let presetner: ProductListPresenter<ProductListViewController> = DIComponent.productListPresenter()
    let searchedProducts = Variable<[Product]>([])
  
    var isLoading = false//if has a search request in loading
    var hasMore = true//if has next page for current search

  
    convenience init(branch: Int){
        self.init()
        self.branch = branch
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        presetner.view = self
    }
    
    func setupView(){
        setupNavBar();
        setupSearchBar();
        setupTableView()
    }
    
    func setupNavBar(){
        self.setLeftBarButtonType(LeftBarButtonTypeScan)
        self.setRightBarButtonTypes([NSNumber(value: RightBarButtonTypeLegacy.rawValue)])
    }
    
    func showPlaceholder(imageName: String, text: String){
        ivPlaceholderImage.image = UIImage(named: imageName)
        lbPlaceholderText.text = text
        placeholderView.isHidden = false
    }
    
    func hidePlaceholder(){
        placeholderView.isHidden = true
    }
    
    
    //MARK: - Load Data
  
    /// load products
    /// if search current text is empty, remove all existing products and skip this search
    /// if is load more and has an existing search request in loading, skip this search
    ///
    /// - Parameter newSearch: true: if search text changed, start a new search
    ///                        false: if search text NOT changed, load next page
    func searchProducts(newSearch: Bool){
        hidePlaceholder()
        if((searchBar.text ?? "").isEmpty){
            searchedProducts.value.removeAll()
            presetner.cancelSearch()
            isLoading = false
            showPlaceholder(imageName: "ic_start_search", text: String.stringWithKey("start_search"))
        }
        
        guard !isLoading, (hasMore || newSearch) else { return }
        if let searchText = searchBar.text,
            !searchText.isEmpty
        {
            isLoading = true;
            startLoading(title: nil)
            if(newSearch){
                searchedProducts.value.removeAll()
            }
            presetner.searchProducts(branch: self.branch!,
                                     start: searchedProducts.value.count,
                                     limit: ProductListViewController.ITEM_PER_PAGE + 1, //send 20 to server, only return 19
                                     searchText: searchText)
        }
    }
    
  
    //MARK: - TableView
    func setupTableView(){
        productsList.rowHeight = 80
        productsList.register(UINib(nibName: ProductListCell.pureClassName(), bundle: nil), forCellReuseIdentifier: ProductListViewController.CELL_IDENTIFIER)
        
        searchedProducts.asObservable()
            .bind(to: productsList.rx.items(cellIdentifier: "ProductListCell", cellType: ProductListCell.self)) {
                (index, product: Product, cell) in
                cell.updateUIWithProduct(product)
                
                //load more
                if(index > self.searchedProducts.value.count - 3){
                    self.searchProducts(newSearch: false)
                }
            }
            .disposed(by: disposeBag)
        
        productsList.rx.itemSelected
            .subscribe{indexPath in
                self.productsList.deselectRow(at: indexPath.element!, animated: true)
                let product = self.searchedProducts.value[indexPath.element!.row]
                self.push(ProductDetailViewController(branch: self.branch!, product:product))
            }
            .disposed(by: disposeBag)
        
        productsList.rx.didScroll
            .subscribe(onNext: { (event) in
                self.searchBar.resignFirstResponder()
                self.searchBar.setShowsCancelButton(false, animated: true)
            })
            .disposed(by: disposeBag)
    }
  
  
    //MARK: - search bar
    func setupSearchBar(){
        searchBar.placeholder = String.stringWithKey("search")
        searchBar.tintColor = .black
        searchBar.barTintColor = UIColor.navigationbarBackground()
        searchBar.layer.borderColor = UIColor.appMain()?.cgColor
        searchBar.layer.borderWidth = 1
        
        let attributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        
        searchBar.rx.text.orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { (text) in
                self.searchProducts(newSearch: true)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .subscribe(onNext: { (event) in
                self.searchBar.resignFirstResponder()
                self.searchProducts(newSearch: true)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.textDidBeginEditing
            .subscribe(onNext: { (event) in
                self.searchBar.setShowsCancelButton(true, animated: true)
            })
            .disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .subscribe(onNext: { (event) in
                self.searchBar.text = ""
                self.searchBar.setShowsCancelButton(false, animated: true)
                self.searchBar.resignFirstResponder()
            })
            .disposed(by: disposeBag)
    }
  
    
    //MARK: - navigation item
    override func leftBarButtonItemTapped(_ btnType: Int) {
        if(btnType == LeftBarButtonTypeScan.rawValue){
            showBarcodeScanner()
        }
    }
    
    override func rightBarButtonItemTapped(_ btnType: Int) {
        if(btnType == RightBarButtonTypeLegacy.rawValue){
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "ViewController")
            (vc as? ViewController)?.delegate = self
            push(vc)
        }
    }
    
    
    //MARK: - Barcode scanner
    func showBarcodeScanner(){
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        viewController.cameraViewController.barCodeFocusViewType = .twoDimensions
        present(viewController, animated: true, completion: nil)
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        controller.dismiss(animated: true, completion: {
            self.push(ProductDetailViewController(branch: self.branch!, barcode: code))
        })
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        showErrorMessage(errorMsg: error.localizedDescription)
    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

    
    //MARK: - ProductListView
    func onProductsSearched(products: [Product]) {
        stopLoading()
        isLoading = false;
        hasMore = products.count >= ProductListViewController.ITEM_PER_PAGE
        searchedProducts.value.append(contentsOf: products)
        
        //no results
        if(products.isEmpty && searchedProducts.value.isEmpty){
            showPlaceholder(imageName: "ic_no_results", text: String.stringWithKey("no_search_result"))
        }
    }
    
    
    //MARK: - Legacy ViewControllerDelegate
    func onScanClicked() {
        self .showBarcodeScanner()
    }
}
