//
//  DIComponent.swift
//  The Warehouse Test
//
//  Created by Johnnie Cheng on 20/01/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

// Depencency Injection Component

import UIKit

class DIComponent: NSObject {
  
    static let api: API = APIImplement()
  
  
    //MARK: Interarctor
    public static func productInteractor() -> ProductInteractor{
        return ProductInteractor(api: api)
    }
  
  
    //MARK: Presenter
    public static func productListPresenter<View: ProductListView>() -> ProductListPresenter<View>{
        return ProductListPresenter<View>(productInteractor: productInteractor())
    }
    
    public static func productDetailPresenter<View: ProductDetailView>() -> ProductDetailPresenter<View>{
        return ProductDetailPresenter<View>(productInteractor: productInteractor())
    }
    
}
